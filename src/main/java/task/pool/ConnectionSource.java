package task.pool;

public interface ConnectionSource<T> {
    T open();
    void close(T obj);
}
