package task.pool;

public class TestConnectionSource implements ConnectionSource<TestConnection> {
    @Override
    public TestConnection open() {
        TestConnection connection = new TestConnection();
        connection.open();
        return connection;
    }

    @Override
    public void close(TestConnection connection) {
        connection.close();
    }
}
