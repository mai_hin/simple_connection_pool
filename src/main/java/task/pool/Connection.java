package task.pool;


public class Connection<T> {
    private T connection;
    private long startIdleTime;

    public Connection(T connection) {
        this.connection = connection;
    }

    public Connection(T connection, long startIdleTime) {
        this.connection = connection;
        this.startIdleTime = startIdleTime;
    }

    public T getConnection() {
        return connection;
    }

    public long getStartIdleTime() {
        return startIdleTime;
    }
}
