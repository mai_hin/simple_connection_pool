package task.pool;

import java.util.concurrent.TimeUnit;

public class PoolFactory<T> implements IPoolFactory<T> {
    @Override
    public IConnectionPool<T> newConnectionPool(int capacity, ConnectionSource<T> src) {
        ConnectionPool<T> connectionPool = new ConnectionPool<T>(src, capacity);
        connectionPool.runDeleterDaemon();
        return connectionPool;
    }

    @Override
    public IConnectionPool<T> newConnectionPool(int init, int max, long keepAliveTime, TimeUnit unit, ConnectionSource<T> src) {
        ConnectionPool<T> connectionPool = new ConnectionPool<T>(src, init, max, keepAliveTime, unit);
        connectionPool.runDeleterDaemon();
        return connectionPool;
    }
}
