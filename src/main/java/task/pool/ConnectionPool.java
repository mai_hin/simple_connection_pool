package task.pool;

import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ConnectionPool<T> implements IConnectionPool<T> {
    private ConnectionSource<T> src;
    private BlockingQueue<Connection<T>> freeConnections;
    private AtomicInteger openConnectionsCount = new AtomicInteger(0);

    private int minimumCount;
    private int maximumCount;
    private long keepAliveTime;
    private TimeUnit timeUnit;
    private Semaphore openConnectionSemaphore = new Semaphore(1);
    private Boolean lockForDeleting = true;


    private void createConnections(ConnectionSource<T> src, int capacity) {
        this.src = src;
        freeConnections = new LinkedBlockingQueue<Connection<T>>();
        for (int i = 0; i < capacity; i++) {
            freeConnections.add(new Connection<T>(src.open()));
            openConnectionsCount.getAndIncrement();
        }
    }

    public ConnectionPool(ConnectionSource<T> src, int initialCount, int maximumCount, long keepAliveTime, TimeUnit timeUnit) {
        createConnections(src, initialCount);
        this.minimumCount = initialCount;
        this.maximumCount = maximumCount;
        this.keepAliveTime = keepAliveTime;
        this.timeUnit = timeUnit;
    }

    public ConnectionPool(ConnectionSource<T> src, int capacity) {
        createConnections(src, capacity);
        this.minimumCount = capacity;
        this.maximumCount = capacity;
    }

    @Override
    public T get() throws InterruptedException {
        T result = tryGet();
        return result == null ? freeConnections.take().getConnection() : result;
    }

    @Override
    public T tryGet() {
        Connection<T> result = freeConnections.poll();
        return tryGetConnection(result);
    }

    @Override
    public T tryGet(long time, TimeUnit unit) throws InterruptedException {
        Connection<T> result = freeConnections.poll(time, unit);
        return tryGetConnection(result);
    }

    private T tryGetConnection(Connection<T> result) {
        if(result != null) {
            return result.getConnection();
        }
        try {
            openConnectionSemaphore.acquire();
            if(openConnectionsCount.get() >= maximumCount) {
                openConnectionSemaphore.release();
                return null;
            }
            openConnectionsCount.incrementAndGet();
            T connection = src.open();
            openConnectionSemaphore.release();
            return connection;

        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void release(T object) {
        synchronized (lockForDeleting) {
            freeConnections.add(new Connection<T>(object, System.currentTimeMillis()));
            if(freeConnections.size() > minimumCount) {
                lockForDeleting.notify();
            }
        }
    }

    public void runDeleterDaemon() {
        Thread deleter = new Thread(() -> {
            synchronized (lockForDeleting) {
                while (true) {
                    try {
                        lockForDeleting.wait();
                        deleteIdleConnections();
                    } catch (InterruptedException e) {
                        break;
                    }
                }
            }
        });
        deleter.setDaemon(true);
        deleter.start();
    }

    private void deleteIdleConnections() {
        for (Iterator<Connection<T>> iterator = freeConnections.iterator(); iterator.hasNext();) {
            Connection<T> connection = iterator.next();
            if(isConnectionIdle(connection)) {
                if(freeConnections.remove(connection)) {
                    openConnectionsCount.decrementAndGet();
                    src.close(connection.getConnection());
                    System.out.println("Close idle connection. Count of open connections is " + openConnectionsCount.get());
                };
            }
            if(openConnectionsCount.get() <= minimumCount) {
                return;
            }
        }
    }

    private boolean isConnectionIdle(Connection connection) {
        long timeStartIdle = TimeUnit.MILLISECONDS.convert(connection.getStartIdleTime(), timeUnit);
        long currentTime = TimeUnit.MILLISECONDS.convert(System.currentTimeMillis(), timeUnit);
        return currentTime - timeStartIdle >= keepAliveTime;
    }

}
