package task.pool;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ConnectionPoolFixedCapacityTest {
    public static void main(String[] args) {
        IPoolFactory<TestConnection> factory = new PoolFactory<>();
        ConnectionSource src = new TestConnectionSource();
        IConnectionPool<TestConnection> pool = factory.newConnectionPool(10, src);
        Runnable r = () -> {
            Random random = new Random();
            while (true) {
                try {
                    switch (random.nextInt(3)) {
                        case 0: {
                            TestConnection connection = pool.get();
                            Thread.sleep(10);
                            pool.release(connection);
                            break;
                        }
                        case 1: {
                            TestConnection connection = pool.tryGet();
                            if (connection == null) break;
                            Thread.sleep(20);
                            pool.release(connection);
                            break;
                        }
                        case 2: {
                            TestConnection connection = pool.tryGet(5, TimeUnit.SECONDS);
                            if (connection == null) break;
                            Thread.sleep(30);
                            pool.release(connection);
                            break;
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        for (int i = 0; i < 20; i++) {
            Thread thread = new Thread(r);
            thread.start();
        }
    }
}




