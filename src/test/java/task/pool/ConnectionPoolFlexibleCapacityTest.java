package task.pool;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ConnectionPoolFlexibleCapacityTest {
    public static void main(String[] args) {
        IPoolFactory<TestConnection> factory = new PoolFactory<>();
        ConnectionSource src = new TestConnectionSource();
        IConnectionPool<TestConnection> pool = factory.newConnectionPool(10, 20, 1, TimeUnit.SECONDS, src);
        Runnable r = () -> {
            Random random = new Random();
            while (true) {
                try {
                    switch (random.nextInt(3)) {
                        case 0: {
                            TestConnection connection = pool.get();
                            Thread.sleep(1000);
                            pool.release(connection);
                            break;
                        }
                        case 1: {
                            TestConnection connection = null;
                            while((connection = pool.tryGet()) == null) {
                                Thread.sleep(100);
                            }
                            Thread.sleep(2000);
                            pool.release(connection);
                            break;
                        }
                        case 2: {
                            TestConnection connection = null;
                            while((connection = pool.tryGet()) == null) {
                                Thread.sleep(100);
                            }
                            Thread.sleep(3000);
                            pool.release(connection);
                            break;
                        }
                    }
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        for (int i = 0; i < 30; i++) {
            Thread thread = new Thread(r);
            thread.start();
        }
    }
}
